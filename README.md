# Code Quality Reporting

This project contains a "custom operation" for the NI LabVIEW CLI that will use VI Analyzer to create a code quality report that can be digested by the GitLab CI chain. It uses a subset of the Code Climate specification.

It is based on the shipping RunVIAnalyzer operation provided by NI, but with some modfiication to add additioanl functionality.

It is covered by an MIT license.